const webpack = require('webpack');

module.exports = {
  configureWebpack: {
    // Set up all the aliases we use in our app.
    plugins: [
      new webpack.optimize.LimitChunkCountPlugin({
        maxChunks: 6,
      }),
    ],
  },
  pwa: {
    name: 'TreeFund DApp',
    themeColor: '#2dce89',
    msTileColor: '#2dce89',
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: '#2dce89',
  },
  css: {
    // Enable CSS source maps.
    sourceMap: process.env.NODE_ENV !== 'production',
  },
};
